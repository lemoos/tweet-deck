// Electron module.
const electron = require('electron')

// The app-behaviour handler.
const app = electron.app

// Module responsible for loading the pages afterwards creating the window.
const BrowserWindow = electron.BrowserWindow

// Module of the menu handling.
const Menu = electron.Menu

// Module which handles the items contained by a Menu.
const MenuItem = electron.MenuItem

// Shell module from Electron.
const shell = electron.shell

const Tray = electron.Tray

// The variable which contains thet main window which is then opened when the app itself initialises.
let mainWindow = null

/** Constructs the main window and loads the URL. */
function createWindow() {
    mainWindow = new BrowserWindow({
        width: 1078, height: 658, title: "TweetDeck is Loading...", center: true, closable: true, resizable: true,
        icon: 'icon.jpg', autoHideMenuBar: true
    });
    mainWindow.loadURL('http://tweetdeck.twitter.com/');

    mainWindow.on('closed', function () {
        mainWindow = null
    });
}

let tray = null

/** Loads after constructing the tray menu */
function loadMenu() {
    const menu = new Menu();
    menu.append(new MenuItem({ label: (mainWindow === null ? 'Open TweetDeck' : 'Refresh TweetDeck'), click() {
        if (mainWindow === null) {
            createWindow();
        } else {
            mainWindow.reload();
        }
    }}));
    menu.append(new MenuItem({ label: 'Open Twitter on Browser', click () {
        shell.openExternal('https://twitter.com/')
    }}));
    if (mainWindow !== null) {
        menu.append(new MenuItem({ label: "Hide/Show TweetDeck", click() {
            if (mainWindow.isVisible()) {
                mainWindow.hide();
            } else {
                mainWindow.show();
            }
        }}));
    }

    if (process.platform === 'darwin') {
        app.dock.setMenu(menu);
        app.dock.setIcon('icon.jpg');
        tray.setTitle('icon.jpg');
        tray.setIgnoreDoubleClickEvents(true);
    }

    tray.setContextMenu(menu);
    tray.setImage('icon.jpg');
    tray.setToolTip('TweetDeck');

    tray.on('click', function () {
        loadMenu();
    });
}

app.on('ready', function () {
    createWindow();
    tray = new Tray('icon.jpg');
    loadMenu();
});
